### macrocell_ICC17

This repository contains the code which has been used in order to generate the simulation results for our article: 

Bonnefoi, R.; Moy, C.; Palicot, J. �New Macrocell Downlink Energy 
Consumption Minimization with Cell DTx and Power Control�, IEEE International Conference on Communications (IEEE ICC) , May 2017.

This article can be found on IEEExplore. A version is also available at: https://hal.archives-ouvertes.fr/hal-01486616

### Short description 

The main file allows to display the average base station power consumption with different values of the capacity constraint.

### License

[MIT Licensed](https://mit-license.org/) (file [LICENSE.txt](LICENSE.txt)).