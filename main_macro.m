%   MAIN_MACRO: main file for the simulation of a base station power
%   consumption with Cell DTx and Power Control
%

%   Other m-files required: opt_sol_Newton.m
%                           PC_only_Newton.m
%                           channelgain_cost231.m
%   Subfunctions: none
%   MAT-files required: none
%
%   See also : none

%   This file is the code used for the simulations done in the paper:
%   Bonnefoi, R.; Moy, C.; Palicot, J. �New Macrocell Downlink Energy 
%   Consumption Minimization with Cell DTx and Power Control�, IEEE 
%   ICC, May 2017.

%   The used base station power consumption model has been established in:
%   G. Auer et al., "How much energy is needed to run a wireless network?," 
%   in IEEE Wireless Communications, vol. 18, no. 5, pp. 40-49, October 2011.

%   Author  : R�mi BONNEFOI
%   SCEE Research Team - CentraleSup�lec - Campus de Rennes
%   Avenue de la Boulaie 35576 Rennes CEDEX CS 47601 FRANCE
%
%   Email : remi.bonnefoi@centralesupelec.fr
%   Website : remibonnefoi.wordpress.com
%
%   Last revision : 07/27/2016

clear all;
close all;
clc;

%%%%%%%%%%%%%%
% PARAMETERS %
%%%%%%%%%%%%%%

% Base station
Ps          = 75000;          % power consumption during sleep mode
P0          = 130000;         % static power consumption
m           = 4.7;            % dynamic power consumption slope
Pmax        = 20000;          % maximum transmit power
B           = 10*10^6;        % band (Hz)
f           = 2000;           % Central frequency

% Users
Cap_const   = 0.1*10^6:0.1*10^6:2*10^6;     % capacity constraint
lambda      = 10;                           % Average number of users in the cell coverage

% Parameters used to plot the results
fail                    = zeros(1,size(Cap_const,2));    % The users can't be served (the base station is unable to satisfy the users' capacit)
nb_Ok                   = zeros(1,size(Cap_const,2));    % number of scenarios OK
Nb_iter                 = 300;                           % Number of iterations used in order to average the result

% Save the sum of the average base station power consumption with the
% different policies.
sum_Ok_min              = zeros(1,size(Cap_const,2));    % With Dtx only (power control is not applied)
sum_Ok_opt              = zeros(1,size(Cap_const,2));    % With the optimal policy
sum_Ok_PC               = zeros(1,size(Cap_const,2));    % With power control only, DTx is not used

% These variables are used in order to evaluate the average service time
% with the two different policies: With DTx only and with the optimal
% policy.
sum_time_min            = zeros(1,size(Cap_const,2));    % With DTx only (power control is not applied)
sum_time_opt            = zeros(1,size(Cap_const,2));    % With the optimal policy

% Analysis of the transmit power
sum_transmit_pow_opt    = zeros(1,size(Cap_const,2));
nb_tot_user             = zeros(1,size(Cap_const,2));

% Number of time the base station is served with power control only
nb_PC_only              = zeros(1,size(Cap_const,2));
tot_nb_iter             = zeros(1,size(Cap_const,2)); %total number of iterations

% These variables are used in order to evaluate the average transmit power
% during a frame.
sum_transpow_min        = zeros(1,size(Cap_const,2));    % With DTx only (power control is not applied)
sum_transpow_opt        = zeros(1,size(Cap_const,2));    % With the optimal policy
sum_transpow_PC         = zeros(1,size(Cap_const,2));    % With power control only, DTx is not used

%%%%%%%%%%%%%
% ALGORITHM %
%%%%%%%%%%%%%

 for i=1:1:size(Cap_const,2)
     while nb_Ok(1,i)<Nb_iter,
        k           = nb_Ok(1,i)+1;
        disp('Capacity constraint:');
        disp(Cap_const(i)/10^6);
        disp('Iteration:');
        disp(k);
        % Number of users
        Nu              =0;
        
        while Nu==0,
            Nu              = poissrnd(lambda);
        end
        
        G_u             = zeros(1,Nu);
        
        % The users are distributed in the cell coverage ( which is a disc.
        % The minimum radius is 300m and the maximum 1500
        d               = zeros(1,Nu);
        for j=1:1:Nu
            x               = unifrnd(300,1500);
            if x>300
                y           = unifrnd(0,sqrt(1500^2-x^2));
            else
                y           = unifrnd(sqrt(300^2-x^2),sqrt(1500^2-x^2));
            end
            
            d(1,j)          = sqrt(x^2+y^2);
        end
        
        % Computation of the channel gain for all users with the Cost 231
        % Hata model
        for j=1:1:Nu
            [G_u(1,j)]      = channelgain_cost231(d(1,j),f,50,1.5);
        end
        
        % Computation of the value of mu_min, the minimum service time
        mu_min                  = Cap_const(1,i)./(B*log2(1+G_u*Pmax/B));
        sum_mu_min              = sum(mu_min);
        
        if sum_mu_min>1,
            fail(1,i)           = fail(1,i)+1;
        else
            % If the sum of the minimum service time is shorter than frame
            % duration, we can serve the users
            
            % We save here the power consumption, service time and average
            % transmit power with the policy which does not use power
            % control
            mean_pow                = (1-sum_mu_min)*Ps+sum_mu_min*(P0+m*Pmax);
            sum_Ok_min(1,i)         = sum_Ok_min(1,i)+mean_pow;
            sum_transpow_min(1,i)   = sum_transpow_min(1,i)+ sum_mu_min*Pmax;
            nb_Ok(1,i)              = nb_Ok(1,i)+1;
            
            % First computation of mu_opt the optimal service time
            mu_opt         = (Cap_const(1,i)*log(2)/B).*(1./(lambertw(exp(-1).*((G_u*(P0-Ps)/(B*m)) -1))+1));
            for j=1:1:Nu
                mu_opt(1,j)     = max(mu_min(1,j),mu_opt(1,j));
            end
            
            % Once we have mu_opt, we have to verify if the constraint on
            % the total service time is met or not
            mu_opt_Newton               = mu_opt;
            
            % Iterative algorithm if this constraint is not met
            if sum(mu_opt_Newton)>1,
                nb_PC_only(1,i)                 = nb_PC_only(1,i)+1;
                [mu_opt_Newton,nb_iter]         = opt_sol_Newton(Nu,mu_opt_Newton,mu_min,B,Cap_const(1,i),G_u,m,Pmax,P0,Ps,0.001);
                tot_nb_iter(1,i)                = tot_nb_iter(1,i)+nb_iter;
            end
            
            % Total service time
            sum_opt_N            = sum(mu_opt_Newton);
            
            transpow_opt                = 0;
            for j=1:1:Nu
                transpow_opt                = transpow_opt+B*(mu_opt_Newton(1,j)/G_u(1,j))*(2^(Cap_const(1,i)/(B*mu_opt_Newton(1,j)))-1);
            end
            
            % We save here the power consumption, service time and average
            % transmit power with the optimal policy
            sum_transpow_opt(1,i)       = sum_transpow_opt(1,i) + transpow_opt;
            sum_Ok_opt(1,i)             = sum_Ok_opt(1,i) + m*transpow_opt+sum_opt_N*P0+(1-sum_opt_N)*Ps;
            sum_time_opt(1,i)           = sum_time_opt(1,i) +sum_opt_N;
            sum_time_min(1,i)           = sum_time_min(1,i) +sum(mu_min);
            
            % Computation of the power consumption with power control only
            [mu_PC]                     = PC_only_Newton(Nu,mu_min,B,Cap_const(1,i),G_u,m,Pmax,0.001);
            
            % We save here the power consumption, service time and average
            % transmit power with the optimal policy without Cell DTx
            transpow_PC                             = 0;
            for j=1:1:Nu
                transpow_PC                         = transpow_PC+ (B*mu_PC(1,j)/G_u(1,j))*(2^(Cap_const(1,i)/(B*mu_PC(1,j)))-1);
            end
            
            sum_transpow_PC(1,i)                    = sum_transpow_PC(1,i) + transpow_PC;
            sum_Ok_PC(1,i)                          = sum_Ok_PC(1,i) +  m*transpow_PC+P0;
        end
    end
 end
 
% Computing the average values
mean_min                = sum_Ok_min./nb_Ok;
mean_opt              = sum_Ok_opt./nb_Ok;
mean_PC                 = sum_Ok_PC./nb_Ok;
mean_transpow_min       = sum_transpow_min./nb_Ok;
mean_transpow_opt       = sum_transpow_opt./nb_Ok;
mean_transpow_PC        = sum_transpow_PC./nb_Ok;
mean_transpow_PC        = mean_transpow_PC/1000;
mean_transpow_min       = mean_transpow_min/1000;
mean_transpow_opt       = mean_transpow_opt/1000;
mean_min                = mean_min/1000;
mean_opt                = mean_opt/1000;
mean_PC                 = mean_PC/1000;
mean_time_min           = sum_time_min./nb_Ok;
mean_time_opt           = sum_time_opt./nb_Ok;


P0plot                  = P0*ones(1,size(mean_min,2));
Psplot                  = Ps*ones(1,size(mean_min,2));
Pmplot                  = (P0+m*Pmax)*ones(1,size(mean_min,2));
P0plot                  = P0plot/1000;
Psplot                  = Psplot/1000;

% number of iterations
mean_number_iter        = tot_nb_iter./nb_PC_only;


%%%%%%%%%%%%%%%%%%%
% DISPLAY RESULTS %
%%%%%%%%%%%%%%%%%%%

% mean transmit power 
figure;
hold on;
grid on;
box on;
plot(Cap_const,mean_min,'-o','LineWidth',1);
plot(Cap_const,mean_opt,'-*','LineWidth',1);
plot(Cap_const,mean_PC,'-v','LineWidth',1);
plot(Cap_const,Psplot,'g','LineWidth',2);
plot(Cap_const,P0plot,'r','LineWidth',2);
ax = gca;
ax.XLim = [0.1*10^6 2*10^6];
xlabel('Capacity constraint per user (bit/s)');
ylabel('Power consumption (W)');
legend('Mean power consumption with \mu_{min}','Optimal mean power consumption \mu_{opt}','Optimal power consumption with PC only','P_s','P_0');
set(findall(gcf,'-property','FontSize'),'FontSize',13) 

figure;
hold on;
grid on;
box on;
plot(Cap_const,mean_time_min,'-o','LineWidth',1);
plot(Cap_const,mean_time_opt,'-*','LineWidth',1);
ax = gca;
ax.XLim = [0.1*10^6 2*10^6];
xlabel('Capacity constraint per user (bit/s)');
ylabel('service time');
legend('mean minimum service time','mean optimal service time');
set(findall(gcf,'-property','FontSize'),'FontSize',13) 

figure;
hold on;
grid on;
box on;
plot(Cap_const,mean_transpow_opt,'-*','LineWidth',1);
plot(Cap_const,mean_transpow_min,'-o','LineWidth',1);
plot(Cap_const,mean_transpow_PC,'-+','LineWidth',1);
ax = gca;
ax.XLim = [0.1*10^6 2*10^6];
xlabel('Capacity constraint per user (bit/s)');
ylabel('Mean transmit power');
legend('With DTx and power control','With DTx only','With power control only');
box on;
grid on;
set(findall(gcf,'-property','FontSize'),'FontSize',13) 